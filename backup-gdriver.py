#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author:  Carlos Guilarte
# Email: carlosguilarte3@gmail.com
# Script para respaldo de proyecto a google Driver

import sys
import logging
import httplib2
from mimetypes import guess_type
import shutil
import os
from time import time, gmtime
import time
import datetime
# sudo pip install --upgrade google-api-python-client
from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from apiclient.errors import ResumableUploadError
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.file import Storage

from lib.termcolor import colored


# Registrar sólo los errores de oauth2client
logging.basicConfig(level="ERROR")

# Ruta de acceso al archivo json token, debe estar en el mismo directorio que el script
token_file = sys.path[0] + '/auth_token.txt'

# Credenciales de la APIs Console
CLIENT_ID = '295331909444-pt6135ptsd2c6v8j6nkt65jusqmh81vv.apps.googleusercontent.com'
CLIENT_SECRET = 'CEsQdXW-Ss6FZaMmqIY4JV19'
# Configuracion de la APIs
OAUTH_SCOPE = 'https://www.googleapis.com/auth/drive.file'
REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob'

# configuracion de usuario de base de datos
username = 'serverq1_adm'
password = 'serverq1_dmo'



# archivos que se respaldara
archivos = ['/home/serverq1/public_html/onefocusdigital',
            '/home/serverq1/public_html/yunielperez',
            '/home/serverq1/public_html/inpietra3',
            '/home/serverq1/sendy.zip',
            '/home/serverq1/send_sms.zip',
            '/home/serverq1/public_html/onefocusdigitalnet',
            '/home/serverq1/public_html/onefocusdigitalnet/sorteo']
# carpetas donde se almacenara en google Driver
capertas_driver = ['0BzF6B60UqhndcTRvYThyN0haa2s',
                   '0BzF6B60UqhndU2RlQUwxeERZLTg',
                   '0BzF6B60UqhndVXNyeUhJa2JlNW8',
                   '0BzF6B60UqhndbzFyN212ei1QMFU',
                   '0BzF6B60UqhndS0hkV3c0ajYtNXc',
                   '0BzF6B60UqhndZ0FadWZjNFFNQW8',
                   '0BzF6B60UqhndZkpDVGRUbWlRcHM']

#base de datos que se respaldaran
db = ['serverq1_onefocusdigital','serverq1_yunielperez', 'serverq1_inpietra3','serverq1_sendy','serverq1_sms_send','serverq1_cch','serverq1_sorteo']




# Get mime type and name of given file
def file_ops(file_path):
    mime_type = guess_type(file_path)[0]
    mime_type = mime_type if mime_type else 'text/plain'
    file_name = file_path.split('/')[-1]
    return file_name, mime_type


def create_token_file(token_file):
# Run through the OAuth flow and retrieve credentials
    flow = OAuth2WebServerFlow(
        CLIENT_ID,
        CLIENT_SECRET,
        OAUTH_SCOPE,
        redirect_uri=REDIRECT_URI
        )
    authorize_url = flow.step1_get_authorize_url()
    print('Vaya al siguiente enlace en su navegador: ' + authorize_url)
    code = raw_input('Ingrese el código de verificación: ').strip()
    credentials = flow.step2_exchange(code)
    storage = Storage(token_file)
    storage.put(credentials)
    return storage


def authorize(token_file, storage):
# Get credentials
    if storage is None:
        storage = Storage(token_file)
    credentials = storage.get()
# Create an httplib2.Http object and authorize it with our credentials
    http = httplib2.Http()
    credentials.refresh(http)
    http = credentials.authorize(http)
    return http



def upload_file(file_path, file_name, mime_type, parent_id, identificador):
# Create Google Drive service instance
    drive_service = build('drive', 'v2', http=http)
# File body description

    media_body = MediaFileUpload(file_path,
                                 mimetype=mime_type,
                                 resumable=True)
    body = {
        'title': file_name,
        'description': 'backup',
        'mimeType': mime_type,
    }

    if parent_id:
         body['parents'] = [{'id': parent_id}]

# Permissions body description: anyone who has link can upload
# Other permissions can be found at https://developers.google.com/drive/v2/reference/permissions
    permissions = {
        'role': 'reader',
        'type': 'anyone',
        'value': None,
        'withLink': True
    }

    #archivo = drive_service.files().get(fileId=''+identificador+'').execute()
    #if archivo:
        # Insert a file
        #file = drive_service.files().delete(fileId=''+identificador+'').execute()
    # Insert new permissions
    #drive_service.permissions().delete(fileId=file['id'], body=permissions).execute()

    # Insert a file
    file = drive_service.files().insert(body=body, media_body=media_body).execute()

# Insert new permissions
    drive_service.permissions().insert(fileId=file['id'], body=permissions).execute()
# Define file instance and get url for download
    file = drive_service.files().get(fileId=file['id']).execute()

    fichero = open ('identificadores.txt', 'a')
    identificador = '%s\n' %(file['id'])
    fichero.write(identificador)
    download_url = file.get('webContentLink')
    return download_url



if __name__ == '__main__':

 totalarchivo = len(archivos)

 identificadores = []
 fichero = open ('identificadores.txt','r')
 for line in fichero:
    identificadores.append(line)

 fichero.close()


 for x in range(len(archivos)):

    if os.path.exists(archivos[x]):

      print colored('\nCreando carpeta y respaldo de base de datos...', 'yellow')

      DATETIME = time.strftime('%m-%d-%Y_%H:%M:%S')
      archivo = 'respaldo_%s' %(DATETIME)
      backup = '/tmp/%s' %(archivo)

      if db[x]:
        dir_bakup_sql = '%s/DB' %(archivos[x])
        eli_dir_backup_sql = 'rm -r %s' %(dir_bakup_sql)

        if os.path.exists(dir_bakup_sql):
          os.popen(eli_dir_backup_sql)

        backup_db = 'mkdir %s && /usr/bin/mysqldump --user=%s --password=%s %s > %s/%s.sql' %(dir_bakup_sql,username, password, db[x], dir_bakup_sql ,db[x])
        os.popen(backup_db)




      print colored('\nComprimiendo proyecto y base de datos...', 'yellow')
      #shutil.get_archive_formats()
      #shutil.make_archive(backup, "zip", archivos[x])
      comprimido = 'zip -P demo4231 -r %s %s' %(backup,archivos[x])
      os.popen(comprimido)

      if os.path.exists(eli_dir_backup_sql):
          print colored('\nEliminando  carpeta de respaldo de base de datos', 'yellow')
          os.popen(eli_dir_backup_sql)

      parent_id = capertas_driver[x]
      file_path = '%s.zip' %(backup)
      identificador = '%s' %(identificadores[x])


      try:
          with open(file_path) as f: pass
      except IOError as e:
          print(e)
          sys.exit(1)
  # Check if token file exists, if not create it by requesting authorization code
      try:
          with open(token_file) as f: pass
      except IOError:
          http = authorize(token_file, create_token_file(token_file))
  # Authorize, get file parameters, upload file and print out result URL for download
      http = authorize(token_file, None)
      file_name, mime_type = file_ops(file_path)

  # Sometimes API fails to retrieve starting URI, we wrap it.
      try:
          print colored('\nSubiendo archivo al servidor de google Driver...', 'yellow')
          upload_file(file_path, file_name, mime_type, parent_id, identificador)
          os.system('clear')
          msj = '\nRespaldo %s de %s listo..' %(x+1, totalarchivo)
          print colored(msj, 'green')

      except ResumableUploadError as e:

          print("Se ha producido un error al intentar la primera subida:", e)
          print("Tratando una vez más.")
          print(upload_file(file_path, file_name, mime_type, parent_id, identificador))
